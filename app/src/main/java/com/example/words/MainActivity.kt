package com.example.words

import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import com.example.words.mvp.MainPresenter
import com.example.words.mvp.MainPresenterViewInterface
import com.example.words.util.KeyboardView
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : MvpAppCompatActivity(), MainPresenterViewInterface {
    private val wordLayouts = ArrayList<View>()

    @InjectPresenter
    lateinit var mPresenter: MainPresenter

    override fun onBackPressed() {
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fullscreen()
        mPresenter.init(this)
    }

    override fun showBoard() {
        if(wordLayouts.isEmpty()) {
            val ll : LinearLayout = findViewById(R.id.layout_words)
            for (i in 1..6) {
                val wl = View.inflate(this, R.layout.word_layout, null)
                wordLayouts.add(wl)
                ll.addView(wl)
            }
            setKeyboardListener()
        }
    }

    override fun showTimer(result : Boolean, word : String) {
        val cl : ConstraintLayout = findViewById(R.id.layout_timer)
        val header : TextView = findViewById(R.id.textView_timer_header)
        val wordView : TextView = findViewById(R.id.textView_timer_word)
        when(result) {
            true -> {
                header.text = getString(R.string.msg_win)
                header.setTextColor(ResourcesCompat.getColor(resources, R.color.win_color, null))
                wordView.setTextColor(ResourcesCompat.getColor(resources, R.color.win_color, null))
            }
            false -> {
                header.text = getString(R.string.msg_defeat)
                header.setTextColor(ResourcesCompat.getColor(resources, R.color.lose_color, null))
                wordView.setTextColor(ResourcesCompat.getColor(resources, R.color.lose_color, null))
            }
        }
        val v = "--$word--"
        wordView.text = v
        cl.visibility = View.VISIBLE
    }

    override fun hideTimer() {
        val cl : ConstraintLayout = findViewById(R.id.layout_timer)
        cl.visibility = View.GONE
    }

    override fun updateTimer(time: Long) {
        val timer : TextView = findViewById(R.id.textView_timer)
        val simpleDateFormat = SimpleDateFormat("HH.mm.ss", Locale.getDefault())
        simpleDateFormat.timeZone = TimeZone.getTimeZone("GMT")
        val dateString = simpleDateFormat.format(time)
        timer.text = dateString
    }

    override fun clearBoard() {
        for(ll in wordLayouts) {
            ll.findViewById<TextView>(R.id.textView_1_1).text = ""
            ll.findViewById<TextView>(R.id.textView_1_1).background.setTintList(null)
            ll.findViewById<TextView>(R.id.textView_1_2).text = ""
            ll.findViewById<TextView>(R.id.textView_1_2).background.setTintList(null)
            ll.findViewById<TextView>(R.id.textView_1_3).text = ""
            ll.findViewById<TextView>(R.id.textView_1_3).background.setTintList(null)
            ll.findViewById<TextView>(R.id.textView_1_4).text = ""
            ll.findViewById<TextView>(R.id.textView_1_4).background.setTintList(null)
            ll.findViewById<TextView>(R.id.textView_1_5).text = ""
            ll.findViewById<TextView>(R.id.textView_1_5).background.setTintList(null)
        }
    }

    override fun inputLetter(word: Int, position: Int, letter: Char) {
        if(wordLayouts.isNotEmpty()) {
            val letterView : TextView = when(position) {
                1 -> wordLayouts[word].findViewById(R.id.textView_1_1)
                2 -> wordLayouts[word].findViewById(R.id.textView_1_2)
                3 -> wordLayouts[word].findViewById(R.id.textView_1_3)
                4 -> wordLayouts[word].findViewById(R.id.textView_1_4)
                else -> { wordLayouts[word].findViewById(R.id.textView_1_5) }
            }
            letterView.text = letter.toString()
        }
    }

    override fun deleteLetter(word: Int, position: Int) {
        val letterView : TextView = when(position) {
            1 -> wordLayouts[word].findViewById(R.id.textView_1_1)
            2 -> wordLayouts[word].findViewById(R.id.textView_1_2)
            3 -> wordLayouts[word].findViewById(R.id.textView_1_3)
            4 -> wordLayouts[word].findViewById(R.id.textView_1_4)
            else -> { wordLayouts[word].findViewById(R.id.textView_1_5) }
        }
        letterView.text = ""
    }

    override fun showError(error: String) = Toast.makeText(this, error, Toast.LENGTH_SHORT).show()

    override fun paintLetter(word: Int, position: Int, color: Int) {
        if(wordLayouts.isNotEmpty()) {
            val letterView : TextView = when(position) {
                1 -> wordLayouts[word].findViewById(R.id.textView_1_1)
                2 -> wordLayouts[word].findViewById(R.id.textView_1_2)
                3 -> wordLayouts[word].findViewById(R.id.textView_1_3)
                4 -> wordLayouts[word].findViewById(R.id.textView_1_4)
                else -> { wordLayouts[word].findViewById(R.id.textView_1_5) }
            }
            letterView.background.setTint(color)
        }
    }

    private fun setKeyboardListener() {
        val keyboardView :KeyboardView = findViewById(R.id.keyboardView)
        keyboardView.setOnKeyboardActionListener(object : KeyboardView.OnKeyboardActionListener {

            override fun onPress(primaryCode: Int) {}
            override fun onRelease(primaryCode: Int) {}

            override fun onKey(primaryCode: Int, keyCodes: IntArray?) {
                mPresenter.onKey(keyboardView.context, primaryCode)
            }

            override fun onText(text: CharSequence?) { }
            override fun swipeLeft() { }
            override fun swipeRight() { }
            override fun swipeDown() { }
            override fun swipeUp() { }
        })
    }

    private fun fullscreen() {
        WindowInsetsControllerCompat(window, window.decorView).let { controller ->
            controller.hide(WindowInsetsCompat.Type.systemBars())
            controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }
        WindowCompat.setDecorFitsSystemWindows(window, false)
    }

}