package com.example.words.util

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ServerClient {

    @FormUrlEncoded
    @POST("WordsGame/splash.php")
    fun getSplash(@Field("locale") locale: String): Call<SplashResponse>

    companion object {
        fun create() : ServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(ServerClient::class.java)
        }
    }

}