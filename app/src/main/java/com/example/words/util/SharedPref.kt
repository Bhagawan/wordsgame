package com.example.words.util

import android.content.Context

class SharedPref {

    companion object {
        fun setBaseWord(context : Context,  word : String) {
            val shP = context.getSharedPreferences("Settings", Context.MODE_PRIVATE)
            shP.edit().putString("0", word).apply()
        }

        fun setWord(context : Context,  word : String) {
            val shP = context.getSharedPreferences("Settings", Context.MODE_PRIVATE)
            shP.edit().putInt("amount", getWordsAmount(context) + 1).putString((getWordsAmount(context) + 1).toString(), word).apply()
        }

        fun getWord(context : Context,num : Int) : String {
            val shP = context.getSharedPreferences("Settings", Context.MODE_PRIVATE)
            return shP.getString(num.toString(), "") ?: ""
        }

        fun setTimer(context : Context, time : Long) {
            val shP = context.getSharedPreferences("Settings", Context.MODE_PRIVATE)
            shP.edit().putLong("time", time).apply()
        }

        fun getLastTime(context : Context) : Long {
            val shP = context.getSharedPreferences("Settings", Context.MODE_PRIVATE)
            return shP.getLong("time", 0)
        }

        fun getWordsAmount(context : Context) : Int {
            val shP = context.getSharedPreferences("Settings", Context.MODE_PRIVATE)
            return shP.getInt("amount", 0)
        }

        fun reset(context : Context) {
            val shP = context.getSharedPreferences("Settings", Context.MODE_PRIVATE)
            shP.edit().clear().apply()
        }

        fun setResult(context : Context, win : Boolean) {
            val shP = context.getSharedPreferences("Settings", Context.MODE_PRIVATE)
            shP.edit().putBoolean("result", win).apply()
        }

        fun getResult(context : Context) : Boolean {
            val shP = context.getSharedPreferences("Settings", Context.MODE_PRIVATE)
            return shP.getBoolean("result", false)
        }
    }
}