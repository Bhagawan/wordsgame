package com.example.words.util

import androidx.annotation.Keep

@Keep
data class SplashResponse(val url : String)

