package com.example.words.mvp

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import com.example.words.util.ServerClient
import com.example.words.util.SplashResponse
import com.squareup.picasso.Picasso
import com.squareup.picasso.Picasso.LoadedFrom
import com.squareup.picasso.Target
import moxy.InjectViewState
import moxy.MvpPresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

@InjectViewState
class SplashPresenter: MvpPresenter<SplashPresenterViewInterface>() {
    private var requestInProcess = false
    lateinit var target : Target

    fun requestSplash() {
        val call : Call<SplashResponse> = ServerClient.create().getSplash(Locale.getDefault().language)
        showLogo()
        call.enqueue(object : Callback<SplashResponse> {
            override fun onResponse(
                call: Call<SplashResponse>,
                response: Response<SplashResponse>
            ) {
                if (requestInProcess) {
                    requestInProcess = false
                    viewState.hideLogo()
                }
                if (response.isSuccessful && response.body() != null) {
                    val s: String = response.body()!!.url
                    if (s == "no") viewState.switchToMain()
                    else viewState.showSplash("https://$s")
                } else {
                    viewState.showError("Server error 2")
                    viewState.switchToMain()
                }
            }

            override fun onFailure(call: Call<SplashResponse>, t: Throwable) {
                viewState.showError("Server error 3")
                viewState.switchToMain()
            }
        })
    }

    private fun showLogo() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: LoadedFrom?) {
                if (requestInProcess) viewState.showLogo(bitmap)
            }
            override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        requestInProcess = true
        Picasso.get().load("http://195.201.125.8/WordsGame/logo.png").into(target)
    }


}