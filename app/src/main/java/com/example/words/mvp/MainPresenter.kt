package com.example.words.mvp

import android.content.Context
import android.os.CountDownTimer
import androidx.core.content.res.ResourcesCompat
import com.example.words.R
import com.example.words.util.Keyboard
import com.example.words.util.SharedPref
import moxy.MvpPresenter

class MainPresenter : MvpPresenter<MainPresenterViewInterface>() {
    private var currentWord : String = ""
    private lateinit var word : String
    private val words = ArrayList<String>()

    fun init(context : Context) {
        loadWords(context)
        if (SharedPref.getWordsAmount(context) > 0) continueGame(context)
        else newGame(context)
    }

    private fun newGame(context : Context) {
        viewState.showBoard()
        viewState.clearBoard()
        viewState.hideTimer()
        SharedPref.reset(context)
        word = words[(words.indices).random()]
        SharedPref.setBaseWord(context, word)
    }

    private fun continueGame(context : Context) {
        viewState.showBoard()
        val wordsAmount = SharedPref.getWordsAmount(context)
        if( wordsAmount > 0) showWords(context)
        if( SharedPref.getLastTime(context) > 0) runTimer(context)
    }

    fun onKey(context : Context, code :Int) = when(code) {
        Keyboard.KEYCODE_DELETE -> {
            deleteLetter(context)
        }
        Keyboard.KEYCODE_DONE -> {
            inputWord(context)
        }
        else -> {
            addLetter(context, code)
        }
    }

    private fun loadWords(context : Context){
        context.resources.openRawResource(R.raw.words).bufferedReader().use {
            words.addAll(it.readLines())
        }
    }

    private fun deleteLetter(context : Context) {
        if(currentWord.isNotEmpty()) {
            viewState.deleteLetter(SharedPref.getWordsAmount(context), currentWord.length)
            currentWord = currentWord.dropLast(1)
        }
    }

    private fun inputWord(context : Context) {
        if(currentWord.length == 5 && wordExist()) {
            SharedPref.setWord(context, currentWord)
            paintWord(context, SharedPref.getWordsAmount(context))
            if(currentWord == SharedPref.getWord(context, 0)) endGame(context, true)
            currentWord = ""
            val amount = SharedPref.getWordsAmount(context)
            if(amount >= 6 && !SharedPref.getResult(context)) endGame(context, false)
        } else viewState.showError(context.getString(R.string.msg_error_no_such_word))
    }

    private fun addLetter(context : Context, code : Int) {
        if(currentWord.length < 5) {
            currentWord += code.toChar()
            viewState.inputLetter(SharedPref.getWordsAmount(context), currentWord.length, code.toChar())
        }
    }

    private fun endGame(context : Context, win : Boolean) {
        SharedPref.setTimer(context, System.currentTimeMillis())
        SharedPref.setResult(context, win)
        runTimer(context)
    }

    private fun runTimer(context : Context) {
        viewState.showTimer(SharedPref.getResult(context), SharedPref.getWord(context, 0))
        val time = when {
            SharedPref.getLastTime(context) > 0 -> 86400000 - (System.currentTimeMillis() - SharedPref.getLastTime(context))
            else -> 86400000
        }
        if(time  >= 1000 ) {
            object : CountDownTimer(time, 1000) {
                override fun onTick(p0: Long) {
                    viewState.updateTimer(p0)
                }

                override fun onFinish() {
                    SharedPref.reset(context)
                    newGame(context)
                }
            }.start()
        } else newGame(context)
    }

    private fun wordExist() : Boolean = currentWord in words

    private fun paintWord(context : Context, num : Int) {
        var place: Boolean
        var letter : Boolean
        val baseWord = SharedPref.getWord(context, 0)
        val wordToCheck = SharedPref.getWord(context, num)
        val byteArr = byteArrayOf(0,0,0,0,0)
        for(n in wordToCheck.indices) {
            place = false
            letter = false
            if(baseWord[n] == wordToCheck[n]) place = true
            if(baseWord.contains(wordToCheck[n])) letter = true

            if(letter && place) byteArr[n] = 2
            else if(letter) byteArr[n] = 1
        }

        for(n in byteArr.indices) {
            if(baseWord.filter { it == wordToCheck[n] }.count() < wordToCheck.filter { it == wordToCheck[n] }.count()) {
                if(n < 3 && byteArr[n] > 0) {
                    for(i in n..3) {
                        if(wordToCheck[n] == wordToCheck[i+1] && byteArr[n] >= byteArr[i+1]) {
                            byteArr[i+1] = 0
                            break
                        } else if(wordToCheck[n] == wordToCheck[i+1] && byteArr[n] < byteArr[i+1]){
                            byteArr[n] = 0
                            break
                        }
                    }
                }
            }
            when(byteArr[n]) {
                (1).toByte() -> viewState.paintLetter(num - 1, n + 1
                    , ResourcesCompat.getColor(context.resources, R.color.letter_half_right, null))
                (2).toByte() -> viewState.paintLetter(num - 1, n + 1
                    , ResourcesCompat.getColor(context.resources, R.color.letter_completely_right, null))
            }
        }

    }

    private fun showWords(context : Context) {
        var word : String
        for(n in 1..SharedPref.getWordsAmount(context)) {
            word = SharedPref.getWord(context, n)
            word.indices.forEach { viewState.inputLetter(n - 1, it + 1, word[it]) }
            paintWord(context, n)
        }
    }
}