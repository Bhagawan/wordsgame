package com.example.words.mvp

import moxy.MvpView
import moxy.viewstate.strategy.alias.*

interface MainPresenterViewInterface : MvpView {

    @AddToEndSingle
    fun showBoard()

    @AddToEndSingle
    fun showTimer(result : Boolean, word : String)

    @AddToEndSingle
    fun hideTimer()

    @OneExecution
    fun updateTimer(time : Long)

    @OneExecution
    fun clearBoard()

    @OneExecution
    fun inputLetter(word : Int, position : Int, letter : Char)

    @OneExecution
    fun deleteLetter(word : Int, position : Int)

    @OneExecution
    fun showError(error : String)

    @OneExecution
    fun paintLetter(word : Int, position : Int, color : Int)
}