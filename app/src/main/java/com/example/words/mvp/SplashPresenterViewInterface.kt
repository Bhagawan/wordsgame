package com.example.words.mvp

import android.graphics.Bitmap
import moxy.MvpView
import moxy.viewstate.strategy.alias.OneExecution
import moxy.viewstate.strategy.alias.SingleState

interface SplashPresenterViewInterface : MvpView {
    @SingleState
    fun switchToMain()

    @OneExecution
    fun showError(error: String?)

    @SingleState
    fun showSplash(url: String?)


    @OneExecution
    fun showLogo(logo: Bitmap?)


    @OneExecution
    fun hideLogo()

}